import React from 'react';
import * as constants from '../constants';

export class LoanHistoryLog extends React.Component {
    render() {
      const { history } = this.props;
      let divArr = [];
      history.forEach(element => {
        divArr.push(
          <div>
            <p>{`${element.id} ${element.action === constants.ACTION_LOAN ? 'took loan of': 'paid'} amount ${element.amount}`}</p>
          </div>
        );
      });
      if(divArr.length > 10) {
        divArr = divArr.slice(divArr.length - 11, divArr.length);
      }
      return (
        <div>
          {divArr}
        </div>
      )
    }
}