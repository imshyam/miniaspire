import React from 'react'
import * as constants from '../constants';

export class GetLoan extends React.Component {

    constructor(props) {
      super(props)
    
      this.state = {
         id: 1,
         amount: 0,
         term: 1,
      }
      this.onIdChange = this.onIdChange.bind(this);
      this.onAmountChange = this.onAmountChange.bind(this);
      this.onTermChange = this.onTermChange.bind(this);
      this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    onIdChange(event){
        console.log("Id: " + event.target.value);
        this.setState({
            id: event.target.value,
        })
    }


    onAmountChange(event){
        console.log("Amount: " + event.target.value);
        this.setState({
            amount: event.target.value,
        })
    }


    onTermChange(event){
        console.log("Term: " + event.target.value);
        this.setState({
            term: event.target.value,
        })
    }

    handleFormSubmit(e){
        e.preventDefault();
        this.props.handleGetLoan(this.state.id, this.state.amount, this.state.term);
        this.setState({
            id: 1,
            amount: 0,
            term: 1,
        });
        // alert("Loan Approved");
    }
    



    render() {
      const amount = parseInt(this.state.amount);
      let fee = amount * constants.INTEREST_RATE / 100; 
      fee = Math.round(fee * 100) / 100;
      const term = parseInt(this.state.term);
      let monthly = amount / term + fee;
      monthly = Math.round(monthly * 100) / 100;
      const total = amount + (fee * term);

      const disableButton = parseInt(this.state.amount) === 0 ? true : false;

      return (
        <div>
          <form>
            <label>Id: </label> <input value={this.state.id} onChange={this.onIdChange} type="number" min="1" /> <br />
            <label>Amount: </label> <input onChange={this.onAmountChange} value={this.state.amount} type="number" min="1" /> < br />
            <label>Loan Term: </label> <select onChange={this.onTermChange} value={this.state.term}>
                <option value="1">1 Month</option>
                <option value="2">2 Months</option>
                <option value="3">3 Months</option>
                <option value="4">4 Months</option>
                <option value="5">5 Months</option>
                <option value="6">6 Months</option>
            </select> < br/>
            <p>Amount: {amount} | Monthly Fee: {fee} </p>
            <p>Monthly Repayment: {monthly}  </p>
            <p>Total Repayment: {total}  </p>
            <button onClick={this.handleFormSubmit} disabled={disableButton}>Get Loan</button>
          </form>
        </div>
      )
    }
}