import React from 'react'

export class PayLoan extends React.Component {

    constructor(props) {
      super(props)

      this.state = {
        id: 0,
        payAmount: 0,
      }

      this.fetchMonthlyPaymentAmount = this.fetchMonthlyPaymentAmount.bind(this);
      this.onIdChange = this.onIdChange.bind(this);
      this.handlePayLoan = this.handlePayLoan.bind(this);
    }

    fetchMonthlyPaymentAmount(e){
        e.preventDefault();
        const { monthlyPay } = this.props;
        if(monthlyPay[this.state.id] === undefined) {
            this.setState({
                payAmount: 0,
            });
            return;
        }
        const allPayments = monthlyPay[this.state.id];
        let totalMonthlyPay = 0;
        allPayments.forEach(element => {
            totalMonthlyPay += element.amount;
        });
        this.setState({
            payAmount: totalMonthlyPay,
        })
    }

    onIdChange(event){
        this.setState({
            id: event.target.value,
        });
    }

    handlePayLoan(e){
        e.preventDefault();
        this.props.handlePayLoan(this.state.id, this.state.payAmount);
        this.setState({
            id: 0,
            payAmount: 0,
        });
    }
    

    render() {
      const styleText = {
          display: this.state.payAmount === 0 ? 'block' : 'none',
      };
      const styleDiv = {
        display: this.state.payAmount === 0 ? 'none' : 'block',
      };
      const disableButton = this.state.payAmount === 0 ? true : false;
      return (
        <div>
          <form>
            <label>Id: </label> <input type="number" min="1" value={this.state.id} onChange={this.onIdChange}  /> <br />
            <button onClick={this.fetchMonthlyPaymentAmount}>Fetch Monthly Repayment Amount</button><br />
            <p style={styleText}>Hurray! No dues.</p>
            <div style={styleDiv}>
                <label>Monthly Repayment Amount: </label> <input disabled={true} type="number" min="1" value={this.state.payAmount} /> < br />
            </div>
            <button disabled={disableButton} onClick={this.handlePayLoan}>Pay</button>
          </form>
        </div>
      )
    }
}