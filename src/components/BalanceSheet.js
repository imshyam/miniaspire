import React from 'react'

export class BalanceSheet extends React.Component {

    render() {
      const { balances } = this.props;
      let rows = [];
      for (var key in balances) {
          const balance = balances[key];
          rows.push(<tr>
            <td>{key}</td>
            <td>{balance.balance}</td>
            <td>{balance.loanAll}</td>
            <td>{balance.loanPaid}</td>
          </tr>);
      }
      return (
        <div>
            <table>
                <tbody>
                    <tr>
                        <th>Id</th>
                        <th>Amount Remaining</th>
                        <th>Total Loan</th>
                        <th>Repayment Completed</th>
                    </tr>
                    {rows}
                </tbody>
            </table>
        </div>
      )
    }
}