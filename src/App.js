import React, { Component } from 'react';
import { GetLoan } from './components/GetLoan';
import { LoanHistoryLog } from './components/LoanHistoryLog';
import { PayLoan } from './components/PayLoan';
import { BalanceSheet } from './components/BalanceSheet';
import * as constants from './constants';

class App extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
       history: [],
       balances: {},
       monthlyPay: {},
    }

    this.handleGetLoan = this.handleGetLoan.bind(this);
    this.handlePayLoan = this.handlePayLoan.bind(this);
  }

  handleGetLoan(id, amount, term) {


    amount = parseInt(amount);
    let fee = amount * constants.INTEREST_RATE / 100;
    fee = Math.round(fee * 100) / 100;
    term = parseInt(term);
    let monthly = amount / term  + fee;
    monthly = Math.round(monthly * 100) / 100;
    const total = amount + (fee * term);

    const history = this.state.history;
    const balances = this.state.balances;
    const monthlyPay = this.state.monthlyPay;
    history.push({
      id: id,
      amount: amount,
      action: constants.ACTION_LOAN,
    })
    if(balances[id] === undefined) {
      balances[id] = {
        balance: total,
        loanAll: total,
        loanPaid: 0,
      };
    } else {
      balances[id] = {
        balance: total + balances[id].balance,
        loanAll: total + balances[id].loanAll,
        loanPaid: balances[id].loanPaid,
      };
    }
    if(monthlyPay[id] === undefined) {
      monthlyPay[id] = [];
    }
    monthlyPay[id].push({
      amount: monthly,
      term: term,
    });
    this.setState({
      history,
      balances,
      monthlyPay,
    })

  }

  handlePayLoan(id, paidAmount) {
    const history = this.state.history;
    const balances = this.state.balances;
    const monthlyPay = this.state.monthlyPay;
    
    paidAmount = Math.round(paidAmount * 100) / 100;

    history.push({
      id: id,
      amount: paidAmount,
      action: constants.ACTION_PAID,
    })
    if(balances[id] === undefined) {
      alert("Somethings' wrong.");
      return;
    } else {
      const newBalance = balances[id].balance - paidAmount;
      const loanPaid = balances[id].loanPaid + paidAmount;
      balances[id].balance = Math.round(newBalance * 100) / 100;
      balances[id].loanPaid = Math.round(loanPaid * 100) / 100;
    }
    if(monthlyPay[id] === undefined) {
      alert("Somethings' wrong.");
      return;
    }
    monthlyPay[id].forEach(element => {
      element.term--;
    });
    monthlyPay[id] = monthlyPay[id].filter(item => item.term > 0);
    if(monthlyPay[id].length === 0) {
      balances[id].loanPaid = balances[id].loanAll;
      balances[id].balance = 0;
    }
    this.setState({
      history,
      balances,
      monthlyPay,
    })
  }

  render() {
    return (
      <div className="parent">
        <div className="topLeft">
          <GetLoan handleGetLoan={this.handleGetLoan} />
        </div>
        <div className="topRight">
          <LoanHistoryLog history={this.state.history} />
        </div>
        <div className="bottomLeft">
          <PayLoan handlePayLoan={this.handlePayLoan} monthlyPay={this.state.monthlyPay} />
        </div>
        <div className="bottomRight">
          <BalanceSheet balances={this.state.balances} />
        </div>
      </div>
    );
  }
}

export default App;
