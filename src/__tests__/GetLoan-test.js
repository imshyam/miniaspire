import React from 'react';
import { shallow, configure } from 'enzyme';
import { GetLoan } from '../components/GetLoan';
import { LoanHistoryLog } from '../components/LoanHistoryLog';
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()});

test('Check Text on GetLoan Component Load', () => {
  const getLoan = shallow(<GetLoan />);
  console.log(getLoan.find('input').at(0));
  const input1 = getLoan.find('input').at(0);
  const input2 = getLoan.find('input').at(1);
  const input3 = getLoan.find('select');
  input1.simulate('change', {target: {value: '1'}});
  input2.simulate('change', {target: {value: '1000'}});
  input3.simulate('change', {target: {value: '1'}});
  expect(getLoan.contains(<p>Amount: 1000 | Monthly Fee: 15 </p>)).toBe(true);
  getLoan.find('select').simulate('click');
  const loanHistoryLog = shallow(<LoanHistoryLog history={[{id: 1, action: 'loan', amount: 1000}]} />);
  expect(loanHistoryLog.contains(<p>1 took loan of amount 1000</p>)).toBe(true);
});