import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import { BalanceSheet } from '../components/BalanceSheet';

configure({adapter: new Adapter()});

test('Check Text on PayLoan Component Load', () => {
  const balanceSheet = shallow(<BalanceSheet />);
    expect(balanceSheet.contains(<th>Id</th>)).toBe(true);
    expect(balanceSheet.contains(<th>Amount Remaining</th>)).toBe(true);
    expect(balanceSheet.contains(<th>Total Loan</th>)).toBe(true);
    expect(balanceSheet.contains(<th>Repayment Completed</th>)).toBe(true);
});