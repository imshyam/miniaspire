import React from 'react';
import { shallow, configure } from 'enzyme';
import { PayLoan } from '../components/PayLoan';
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()});

test('Check Text on PayLoan Component Load', () => {
  const payLoan = shallow(<PayLoan />);
  const styleText = {
    display: 'block',
  };
  expect(payLoan.contains(<p style={styleText}>Hurray! No dues.</p>)).toBe(true);
});